##### Unattended ISO 

This script will create an unattended Ubuntu ISO or CentOS ISO without any human intervention. There are four arguments passed to the script when it is executed, 

1. It is for path to the ISO image, 
2. It is for kickstart file
3. It is for type of ISO(Ubuntu or CentOS),
4. It is for preseed file path(which is not required in case of CentOS).

**For ubuntu:**

```shell
sudo bash created-unattended-iso.sh /path/to/ubuntu-ISO ~/ks-ubuntu.cfg ubuntu ~/ubuntu-auto.seed
```

**For CentOS:**

```shell
sudo bash created-unattended-iso.sh /path/to/CentOS-ISO ~/ks-centos.cfg centos
```

**Kickstart Configuration File**

This file contain all the configuration that is required during the installation. `

**Download ISO images**

For Ubuntu:

http://releases.ubuntu.com/16.04/

For CentOS

https://www.centos.org/download/

##### Compatibility 

The scripts supports the following editions

- Ubuntu 16.04 Server LTS amd64
- CentOS-7-x86_64

