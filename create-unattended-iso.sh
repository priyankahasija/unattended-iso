#!/bin/bash
#
#
# For the creation of unattended iso 
#
#----------------------------------
# SETTING VARIABLES
#----------------------------------

ISO_FILE_PATH=$1
KICKSTART_FILE_PATH=$2
PRESEED_FILE_PATH=$3
ISO_NAME=$4

#----------------------------------------
# MOUNT THE ISO AND SET RIGHT PERMISSIONS
#----------------------------------------

mkdir mnt
sudo mount -r -o loop $ISO_FILE_PATH mnt
mkdir iso_files
rsync -a mnt/ iso_files/
chmod 755 iso_files
umount mnt
rm -rf mnt

#-----------------------------------
# VALIDATE THE KICKSTART CONFIG FILE
#-----------------------------------

sudo apt install python-pykickstart
ksvalidator $KICKSTART_FILE_PATH

#----------------------------------
# EDITING THE CONTENTS OF ISO IMAGE
#----------------------------------

if [$ISO_NAME == 'ubuntu' && $? -eq 0]
then
    cp {$KICKSTART_FILE_PATH,$PRESEED_FILE_PATH} iso_files
    chmod 644 iso_files/*.cfg iso_files/*.seed
    chmod 755 iso_files/isolinux iso_files/isolinux/txt.cfg iso_files/isolinux/isolinux.cfg
    sed -i '/^\default install$/a label autoinstall\n\ \ menu label ^Automatically install Ubuntu\n\ \ kernel /install/vmlinuz\n\ \ append file=/cdrom/preseed/ubuntu-server.seed vga=788 initrd=/install/initrd.gz ks=cdrom:/ks.cfg preseed/file=/cdrom/ubuntu-auto.seed quiet --' iso_files/isolinux/txt.cfg
    chmod 555 iso_files/isolinux
    chmod 444 iso_files/isolinux/txt.cfg iso_files/isolinux/isolinux.cfg
else
    cp {$KICKSTART_FILE_PATH} iso_files
fi

#-----------------------------------------------------
# CREATING NEW ISO IMAGE IN PRESENT WORKING DIRECTORY
#-----------------------------------------------------

if [$ISO_NAME == 'ubuntu']
then
   mkisofs -D -r -V "ubuntu-auto" -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset utf-8 -cache-inodes -quiet -o ubuntu-auto.iso iso_files/
else
   mkisofs -D -r -V "centos-auto" -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -input-charset utf-8 -cache-inodes -quiet -o centos-auto.iso iso_files/
fi

#--------
#CLEANUP
#--------

rm -rf iso_files
